<?php

  session_start();
    
  // error_reporting(E_ALL);
  // ini_set('display_errors', 'On');

  DEFINE ('DBUSER', 'remote');
  DEFINE ('DBPASS', 'GQ8XEQAVgwvVL8EpTUCaVS6S');
  DEFINE ('DBHOST', '107.170.7.163');
  DEFINE ('DBNAME', 'niche_finder');

  $dbo = new PDO('mysql:host=' .DBHOST. ';dbname=' .DBNAME, DBUSER, DBPASS);

  if($_GET['action'] == 'signup_confirmation') { //from email link...

    $email = $_GET['email'];
    $token = $_GET['token'];

    $query = $dbo->prepare("SELECT id FROM users WHERE email = ? AND token = ?");
    $query->execute(array($email, $token));

    $result = $query->fetchAll();

    foreach($result as $row) {

      $query = $dbo->prepare("UPDATE users SET verified = ? WHERE email = ? AND token = ?");
      $query->execute(array(1, $email, $token));

      header('Location: /?ref=email_confirmed#account_page');
      exit;
    }

    header('Location: /?e=invalid_confirmation_link#account_page');
    exit;

  }


  if($_GET['action'] == 'logout') {

    session_unset();
    session_destroy();

    header('Location: /');
    exit;
  }

  if($_GET['action'] == 'password_reset') { //from email link...

    $email = $_GET['email'];
    $token = $_GET['token'];

    $query = $dbo->prepare("SELECT id FROM users WHERE email = ? AND token = ?");
    $query->execute(array($email, $token));

    $result = $query->fetchAll();

    foreach($result as $row) {

      $valid = true;
    }

    if(!$valid) {

      header('Location: /?e=invalid_password_reset_request#account_page');
      exit;
    }
  }



  if($_GET['action'] == 'resend_confirmation') {

    $email = $_POST['email'];

    $query = $dbo->prepare("SELECT token FROM users WHERE email = ?");
    $query->execute(array($email));

    $result = $query->fetchAll();

    foreach($result as $row) {

      $token = $row[0];
    }

    if($token) {

      //Email Confirmation
      require_once('sendgrid-api/vendor/autoload.php');

      $sendgrid = new SendGrid('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', array("turn_off_ssl_verification" => true));
      $mail    = new SendGrid\Email();

      $mail->addTo($email)->
             setFrom('noreply@amznichefinder.com')->
             setFromName('AMZNicheFinder')->
             setSubject('AMZNicheFinder.com – Please Confirm Your Email Address')->
             setHtml('Thank you for registering at AMZNicheFinder.com.<br><br>To complete your account registration, please confirm your email address by visiting the following link:<br><br>
                    <a href="http://amznichefinder.com/?action=signup_confirmation&email=' . $email . '&token=' . $token . '#account_page">http://amznichefinder.com/?action=signup_confirmation&email=' . $email . '&token=' . $token. '#account_page</a><br><br>
                    Best wishes,<br>
                    AMZNicheFinder');

      $response = $sendgrid->send($mail);

      header('Location: /?ref=confirmation_sent#reset');
      exit;

    } else {

      header('Location: /?e=email_not_found#reset');
      exit;
    }
  }

  if($_GET['action'] == 'request_password_reset') {

    $email = $_POST['email'];

    $query = $dbo->prepare("SELECT token FROM users WHERE email = ?");
    $query->execute(array($email));

    $result = $query->fetchAll();

    foreach($result as $row) {

      $token = $row[0];
    }

    if($token) {

      //Email Confirmation
      require_once('sendgrid-api/vendor/autoload.php');

      $sendgrid = new SendGrid('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', array("turn_off_ssl_verification" => true));
      $mail    = new SendGrid\Email();

      $mail->addTo($email)->
             setFrom('noreply@amznichefinder.com')->
             setFromName('AMZNicheFinder')->
             setSubject('AMZNicheFinder.com – Password Reset')->
             setHtml('Please follow the link below in order to reset your password.<br><br>
                    <a href="http://amznichefinder.com/?action=password_reset&email=' . $email . '&token=' . $token . '#reset">http://amznichefinder.com/?action=password_reset&email=' . $email . '&token=' . $token. '#reset</a><br><br>
                    Best wishes,<br>
                    AMZNicheFinder');

      $response = $sendgrid->send($mail);

      header('Location: /?ref=password_reset_request_sent#reset');
      exit;

    } else {

      header('Location: /?e=email_not_found#reset');
      exit;
    }
  }

  if($_GET['action'] == 'signin') {

    $email = $_POST['signin_email'];
    $password = $_POST['signin_password'];

    if($email == '' || $password == '') {

      header('Location: /?e=signin_missing_fields#account_page');
      
    
    } else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

      header('Location: /?e=signin_invalid_email#account_page');
      
    }

    $password_hashed = hash('SHA1', $password);

    $query = $dbo->prepare("SELECT id, membership_status, verified, expiry_date FROM users WHERE email = ? AND password = ?");
    $query->execute(array($email, $password_hashed));

    $result = $query->fetchAll();
    
    foreach($result as $row) {

      $verified = $row[2];

      if($verified == 1) {

        $_SESSION['user_id'] = $row[0];
        $_SESSION['email'] = $email;
        $_SESSION['logged_in'] = true;
        $_SESSION['membership_status'] = $row[1];
        $_SESSION['expiry_date'] = $row[3];

        header('Location: /#account_page');
        exit;

      } else {

        header('Location: /?e=inactive#account_page');
        exit;
      }

    }

    header('Location: /?e=incorrect_login#account_page');
    exit;
  
  } else if($_GET['action'] == 'signup') {

    $email = $_POST['signup_email'];
    $password = $_POST['signup_password'];

    if($email == '' || $password == '') {

      header('Location: /?e=signup_missing_fields#account_page');
      exit;
    
    } else if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {

      header('Location: /?e=signup_invalid_email#account_page');
      exit;
    }

    $password_hashed = hash('SHA1', $password);

    $token = hash('SHA256', time());

    $datetime = date('Y-m-d H:i:s');

    $query = $dbo->prepare("SELECT COUNT(*) FROM users WHERE email = ?");
    $query->execute(array($email));

    $result = $query->fetchAll();

    foreach($result as $row) {

      $num_rows = $row[0];
    }

    if($num_rows == 0) {

      $query = $dbo->prepare("INSERT INTO users SET email = ?, password = ?, date_joined = ?, token = ?");
      $query->execute(array($email, $password_hashed, $datetime, $token));

      $user_id = $dbo->lastInsertId();


      //Email Confirmation
      require_once('sendgrid-api/vendor/autoload.php');

      $sendgrid = new SendGrid('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', array("turn_off_ssl_verification" => true));
      $mail    = new SendGrid\Email();

      $mail->addTo($email)->
             setFrom('noreply@amznichefinder.com')->
             setFromName('AMZNicheFinder')->
             setSubject('AMZNicheFinder.com – Please Confirm Your Email Address')->
             setHtml('Thank you for registering at AMZNicheFinder.com.<br><br>To complete your account registration, please confirm your email address by visiting the following link:<br><br>
                    <a href="http://amznichefinder.com/?action=signup_confirmation&email=' . $email . '&token=' . $token . '#account_page">http://amznichefinder.com/?action=signup_confirmation&email=' . $email . '&token=' . $token. '#account_page</a><br><br>
                    Best wishes,<br>
                    AMZNicheFinder');

      $response = $sendgrid->send($mail);
      

      header('Location: /?ref=confirm_email#account_page');
      exit;

    } else {

      //Email in use...
      header('Location: /?e=email_in_use#account_page');
      exit;

    }
  }


  if($_GET['action'] == 'cancel') {

    //cancel subscription

    $query = $dbo->prepare("SELECT paypal_id FROM transactions WHERE user_id = ?");
    $query->execute(array($_SESSION['user_id']));

    $result = $query->fetchAll();

    foreach($result as $row) {

      $paypal_id = $row[0];
    }

    $url = 'https://api-3t.paypal.com/nvp';
    //$url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

    //Confirm with paypal that the IPN was received....
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "VERSION=98&METHOD=ManageRecurringPaymentsProfileStatus&USER=amznichefinder_api1.gmail.com&PWD=A8NWBDFFBWKQ7L8N&SIGNATURE=AiPC9BjkCyDFQXbSkoZcgqH3hpacA-OBdAaNFXqfxnUHFPJmh2MHgj5r&PROFILEID=" . $paypal_id . "&ACTION=Cancel");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    $res = curl_exec($ch);
    
    curl_close($ch);

    $res_arr = array();
    parse_str($res, $res_arr);
  
    if($res_arr['ACK'] == 'Success') {

        $_SESSION['membership_status'] = 0;

        header('Location: /?ref=cancelled#account_page');
    
    } else if($res_arr['ACK'] == 'Failure'){

        header('Location: /?ref=cancel_failed#account_page');
    }

  }


  if($_SESSION['logged_in'] == true) {


  }

?>
<!DOCTYPE html>
<html>
    <head>
      <title>AMZNicheFinder 2.0 - Amazon Niche Finder & Research Tool</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <meta property="og:image" content="http://amznichefinder.com/og_artwork.png"/>

      <!-- Mobile support -->
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <!-- Material Design fonts -->
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

      <!-- Data Tables -->
      <link href="http://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet">

      <!-- Bootstrap -->
      <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

      <!-- Bootstrap Material Design -->
      <link href="css/bootstrap-material-design.css" rel="stylesheet">
      <link href="css/ripples.min.css" rel="stylesheet">

      <!-- Dropdown.js -->
      <link href="http://cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.css" rel="stylesheet">

      <!-- Page style -->
      <link href="css/custom.css" rel="stylesheet">

      <!-- jQuery -->
      <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>

      <link rel="icon" type="image/png" href="favicon.png" />

    </head>
    <body>
      <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-73017237-1', 'auto');
      ga('send', 'pageview');

    </script>

        <div class="header-panel shadow-z-2">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-3">
                <h1><a href="http://amznichefinder.com/"><img src="logo_wh.png" alt="AMZNicheFinder" /></a></h1>
              </div>
            </div>
          </div>
        </div>
        <div class="container-fluid main">
          <div class="row">
            <nav class="col-xs-3 col-md-2 menu">
              <ul>
                <li class="withripple" data-target="#table_view" id="2619526011">Appliances</li>
                <li class="withripple" data-target="#table_view" id="2617942011">Arts, Crafts & Sewing</li>
                <li class="withripple" data-target="#table_view" id="15690151">Automotive</li>
                <li class="withripple" data-target="#table_view" id="165797011">Baby</li>
                <li class="withripple" data-target="#table_view" id="11055981">Beauty</li>
                <li class="withripple" data-target="#table_view" id="1000">Books</li>
                <li class="withripple" data-target="#table_view" id="2335753011">Cell Phones & Accessories</li>
                <li class="withripple" data-target="#table_view" id="7141124011">Clothing, Shoes & Jewelry</li>
                <!-- <li class="withripple" data-target="#table_view" id="541966">Computers</li> -->
                <li class="withripple" data-target="#table_view" id="493964">Electronics</li>
                <li class="withripple" data-target="#table_view" id="16310211">Grocery & Gourmet Food</li>
                <li class="withripple" data-target="#table_view" id="3760931">Health & Personal Care</li>
                <li class="withripple" data-target="#table_view" id="1063498">Home & Kitchen</li>
                <li class="withripple" data-target="#table_view" id="468240">Home Improvement</li>
                <li class="withripple" data-target="#table_view" id="16310161">Industrial & Scientific</li>
                <li class="withripple" data-target="#table_view" id="11965861">Musical Instruments</li>
                <li class="withripple" data-target="#table_view" id="1084128">Office Products</li>
                <li class="withripple" data-target="#table_view" id="3238155011">Patio, Lawn & Garden</li>
                <li class="withripple" data-target="#table_view" id="2619534011">Pet Supplies</li>
                <li class="withripple" data-target="#table_view" id="3375301">Sports & Outdoors</li>
                <li class="withripple" data-target="#table_view" id="165795011">Toys & Games</li>
              </ul>
            </nav>
            <div class="pages col-xs-9">
                  <div class="row">
                    <div class="col-xs-11">
                      <div class="well page" id="about">

                        <img src="logo_blk.png" alt="AMZNicheFinder" />

                        <br><br>
                        <div class="alert alert-success">Now Active: Improved <strong>Estimated Sales</strong> Algorithm. See it for yourself!</div>


                        <h3><strong>Find untapped niche markets on Amazon.</h3></h3>

                        <p>Our service is completely in-house designed and optimized based on numerous hours of research.<br>Even still, we continue to make adjustments to further improve the accuracy of our algorithm.<br>
                        As it stands, it is the only tool of its kind available on the web.</p>

                        <br>
                        <h3><strong>How does AMZNicheFinder work?</strong></h3>

                        <p>Our algorithm takes into consideration the following 6 distinct criteria:</p>

                        <ul>
                          <li>Sales Rank</li>
                          <li>Est. Sales</li>
                          <li>Competition (# of products in any given subcategory)</li>
                          <li>Product Prices</li>
                          <li># of Reviews</li>
                          <li>Rating</li>
                        </ul>

                        <p>Using our in-house tools, we gather the above information for all of the relevant subcategories on Amazon (<strong>some 20,000+ subcategories!</strong>)</p>

                        <p>Each criterion is then given a weight based on its importance, so that the total possible weight/score of all of them combined is between 0 and 10.</p>

                        <br>
                        <h3><strong>Pricing</strong></h3>

                        <p>AMZNicheFinder is free to use as a guest with limited results. Full access to this service is available for <strong><strike>$35USD</strike> $19USD/month</strong>, and your membership can be cancelled at any time.<br>
                       <strong><a href="#account_page" data-target="#account_page"> Sign Up for a new account to get started</a></strong>.</p>

                        <p><strong>Please keep in mind that this service is experimental and our services are undergoing continuous development. The data may not always be 100% accurate. Please use at your own discretion.</strong></p>

                        <br>
                        <h3><strong>Note:</strong> As a guest user, you are able to view niches with scores between 0-7. To view niches with scores above 7, please <a href="#account_page" data-target="#account_page"><strong>Log In / Sign Up</strong></a>.</h3>
                        
                        <br>
                        <p>Support: <a href="mailto:amznichefinder.com">amznichefinder@gmail.com</a></p>

                        <br>
                        <p>This site is not affiliated with Amazon Inc. in any way.</p>
                      </div>

                      <div class="well page" id="account_page">
                        
                        <div class="container">

                          <div class="row">
                            <div class="col-xs-12">

                            <?php

                              if($_GET['e'] == 'incorrect_login') {

                                echo '<span class="alert alert-danger">The provided email/password combination is incorrect. Please try again.</span>';
                              
                              } else if($_GET['e'] == 'inactive') {

                                echo '<span class="alert alert-danger">Please confirm your email to activate your account.</span>';
                              
                              } else if($_GET['e'] == 'signin_missing_fields') {

                                echo '<span class="alert alert-danger">Please provide your email and password in order to sign in.</span>';
                              
                              } else if($_GET['e'] == 'signin_invalid_email') {

                                echo '<span class="alert alert-danger">Please provide a valid email address.</span>';
                              
                              } else if($_GET['e'] == 'signup_missing_fields') {

                                echo '<span class="alert alert-danger">Please provide your email and password.</span>';
                              
                              } else if($_GET['e'] == 'signup_invalid_email') {

                                echo '<span class="alert alert-danger">Please provide a valid email address.</span>';
                              
                              } else if($_GET['e'] == 'email_in_use') {

                                echo '<span class="alert alert-danger">The provided email address is already in use. Please provide another one or sign in.</span>';
                              
                              } else if($_GET['e'] == 'invalid_password_reset_request') {

                                echo '<span class="alert alert-danger">Invalid Password Reset Request. Please make sure you follow the link provided in the email we have sent you.</span>';
                              
                              } else if($_GET['e'] == 'invalid_confirmation_link') {

                                echo '<span class="alert alert-danger">Invalid Confirmation Request. Please make sure you follow the link provided in the email we have sent you.</span>';
                              
                              } else if($_GET['ref'] == 'confirm_email') {

                                echo '<span class="alert alert-success">Success! Your account has been created. Please follow the link in the confirmation email we have sent you in order to activate your account. Please be sure to check your spam/junk folder just in case.</span>';
                              
                              } else if($_GET['ref'] == 'email_confirmed') {

                                echo '<span class="alert alert-success">Success! Your account has been confirmed. Please proceed to Sign in.</span>';
                              
                              } else if($_GET['ref'] == 'payment_success') {

                                echo '<span class="alert alert-success">Success! Your Pro Membership is now active!</span>';
                              
                              }

                            ?>

                            </div>
                          </div>

                          <?php

                            if($_SESSION['logged_in'] == true) {

                              $now = time();

                              if($_SESSION['membership_status'] == 1) {

                                  $status = 'Active';

                              } else if($_SESSION['membership_status'] == 0 && strtotime($_SESSION['expiry_date']) > $now) {

                                  $status = 'Pending Cancellation';
                              
                              } else {

                                  $status = 'Inactive';
                              }

                          ?>

                            <div class="row">
                                <div class="col-xs-12">

                                  <h3>Welcome back! <a href="?action=logout" class="pull-right btn btn-default m-t-0">Sign Out</a></h3>

                                  <br>
                                  <p>Subscription Status: <strong><?php echo $status; ?></strong></p>
                                  <p>Expiry Date: <?php echo ($_SESSION['expiry_date'] != '0000-00-00 00:00:00') ? '<strong>' . date('m j, Y', strtotime($_SESSION['expiry_date'])) . '</strong>' : '<strong>N/A</strong>'; ?></p>

                                  <?php

                                    if($_SESSION['membership_status'] == 0) {

                                  ?>

                                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                                    <input type="hidden" name="cmd" value="_s-xclick">
                                    <input type="hidden" name="hosted_button_id" value="4BSR7PDBDXMDS">
                                    <input type="hidden" name="custom" value="<?php echo $_SESSION['email']; ?>" />
                                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                    </form>

                                    <p><strong><strike>$35USD</strike> $19USD/month</strong></p>

                                  <?php 

                                    } else {

                                  ?>

                                  <br>
                                  <a href="?action=cancel" class="btn btn-default m-t-0">Cancel Subscription</a>

                                  <?php

                                    }

                                  ?>

                                </div>
                            </div>

                          <?php

                            } else {

                          ?>
                            <div class="row">
                                <div class="col-sm-6 col-md-6">

                                    <h3>Sign In</h3>

                                    <form action="?action=signin" method="POST" class="form-horizontal">
                                      <fieldset>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="signin_email">Email</label>  
                                        <div class="col-md-6">
                                        <input id="signin_email" name="signin_email" type="text" placeholder="Email Address" class="form-control input-md" required="">
                                          
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="signin_password">Password</label>  
                                        <div class="col-md-6">
                                        <input id="signin_password" name="signin_password" type="password" placeholder="Password" class="form-control input-md" required="">
                                          
                                        </div>
                                      </div>

                                      <!-- Button -->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="signin"></label>
                                        <div class="col-md-6">
                                          <input type="submit" id="signin" name="signin" class="btn btn-primary" value="Sign In" />
                                        </div>
                                      </div>

                                      </fieldset>
                                    </form>

                                    <a href="#reset" data-target="#reset" class="pull-right">Reset Password</a> 
                                </div>

                                <div class="col-sm-6 col-md-6">

                                    <h3>Sign Up</h3>

                                    <form action="?action=signup" method="POST" class="form-horizontal">
                                      <fieldset>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="signup_email">Email</label>  
                                        <div class="col-md-6">
                                        <input id="signup_email" name="signup_email" type="text" placeholder="Email Address" class="form-control input-md" required="">
                                          
                                        </div>
                                      </div>

                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="signup_password">Password</label>  
                                        <div class="col-md-6">
                                        <input id="signup_password" name="signup_password" type="password" placeholder="Password" class="form-control input-md" required="">
                                          
                                        </div>
                                      </div>

                                      <!-- Button -->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="signup"></label>
                                        <div class="col-md-6">
                                          <input type="submit" id="signup" name="signup" class="btn btn-primary" value="Sign Up" />
                                        </div>
                                      </div>

                                      </fieldset>
                                    </form>

                                    <a href="#reset" data-target="#reset" class="pull-right">Resend Confirmation</a> 
                                </div>
                            </div>

                          <?php

                            }

                          ?>

                            <br><br>
                            <div class="row">
                              <div class="col-xs-12">

                                <p>Disclaimer: AMZNicheFinder is an experimental service and is undergoing continuous development. Keep in mind that the data are estimations and can be inaccurate at times. There are no full or partial refunds, but you may cancel your membership at any time.</p>

                              </div>
                            </div>
                        </div>
                        
                      </div>

                      <?php

                        if(!isset($_SESSION['logged_in'])) {

                      ?>

                        <div class="well page" id="reset">

                          <div class="container">


                            <div class="row">
                              <div class="col-xs-12">

                              <?php

                                if($_GET['ref'] == 'password_reset_request_sent') {

                                  echo '<span class="alert alert-success">Please follow the link in the email we have sent you in order to reset your password. Please be sure to check your spam/junk folder just in case.</span>';
                                
                                } else if($_GET['ref'] == 'confirmation_sent') {

                                  echo '<span class="alert alert-success">Confirmation email has been sent. Please be sure to check your spam/junk folder just in case.</span>';
                                
                                } else if($_GET['e'] == 'email_not_found') {

                                  echo '<span class="alert alert-danger">The specified email address was not found. Please try again or sign up for a new account.</span>';
                                }

                              ?>

                              </div>
                            </div>

                            <?php

                              if($_GET['action'] == 'password_reset') {

                            ?>

                              <div class="row">
                                  <div class="col-sm-6 col-md-6">

                                      <h3>Set New Password</h3>

                                      <form action="?action=confirm_password_change&email=<?php echo $_GET['email'] . '&' . $_GET['token']; ?>" method="POST" class="form-horizontal">
                                        <fieldset>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="new_password">New Password</label>  
                                          <div class="col-md-6">
                                            <input id="new_password" name="new_password" type="password" placeholder="New Password" class="form-control input-md" required="">
                                          </div>
                                        </div>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="new_password">Confirm Password</label>  
                                          <div class="col-md-6">
                                            <input id="confirm_password" name="confirm_password" type="password" placeholder="Confirm Password" class="form-control input-md" required="">
                                          </div>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="signin"></label>
                                          <div class="col-md-6">
                                            <input type="submit" class="btn btn-primary" value="Set Password" />
                                          </div>
                                        </div>

                                        </fieldset>
                                      </form>
                                  </div>
                              </div>

                            <?php

                              } else {

                            ?>

                              <div class="row">
                                  <div class="col-sm-6 col-md-6">

                                      <h3>Reset Password</h3>

                                      <form action="?action=request_password_reset" method="POST" class="form-horizontal">
                                        <fieldset>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="signin_email">Email</label>  
                                          <div class="col-md-6">
                                          <input id="password_reset_email" name="email" type="text" placeholder="Email Address" class="form-control input-md" required="">
                                            
                                          </div>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="signin"></label>
                                          <div class="col-md-6">
                                            <input type="submit" class="btn btn-primary" value="Request Reset" />
                                          </div>
                                        </div>

                                        </fieldset>
                                      </form>
                                  </div>

                                  <div class="col-sm-6 col-md-6">

                                      <h3>Resend Confirmation Email</h3>

                                      <form action="?action=resend_confirmation" method="POST" class="form-horizontal">
                                        <fieldset>

                                        <!-- Text input-->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="signup_email">Email</label>  
                                          <div class="col-md-6">
                                          <input id="resend_confirmation_email" name="email" type="text" placeholder="Email Address" class="form-control input-md" required="">
                                            
                                          </div>
                                        </div>

                                        <!-- Button -->
                                        <div class="form-group">
                                          <label class="col-md-4 control-label" for="signup"></label>
                                          <div class="col-md-6">
                                            <input type="submit"class="btn btn-primary" value="Resend Confirmation" />
                                          </div>
                                        </div>

                                        </fieldset>
                                      </form>
                                  </div>
                              </div>

                            <?php

                              }

                            ?>
                        </div>

                        </div>

                      <?php

                        }

                      ?>

                      <div class="well page" id="table_view">

                        <table class="table table-bordered table-striped filter_options">
                          <tr>
                            <th rowspan="2" valign="middle">Filter</th>
                            <th>Avg. BSR</th>
                            <th>Est. Sales</th>
                            <th># Products</th>
                            <th>Avg. Price</th>
                            <th>Avg. Reviews</th>
                            <th>Avg. Rating</th>
                            <th>Score</th>
                          </tr>
                          <tr>
                            <td>
                              <input type="text" id="bsr_min" placeholder="Min" />&nbsp;
                              <input type="text" id="bsr_max" placeholder="Max" />
                            </td>
                            <td>
                              <input type="text" id="sales_min" placeholder="Min" />&nbsp;
                              <input type="text" id="sales_max" placeholder="Max" />
                            </td>
                            <td>
                              <input type="text" id="products_min" placeholder="Min" />&nbsp;
                              <input type="text" id="products_max" placeholder="Max" />
                            </td>
                            <td>
                              <input type="text" id="price_min" placeholder="Min" />&nbsp;
                              <input type="text" id="price_max" placeholder="Max" />
                            </td>
                            <td>
                              <input type="text" id="reviews_min" placeholder="Min" />&nbsp;
                              <input type="text" id="reviews_max" placeholder="Max" />
                            </td>
                            <td>
                              <input type="text" id="rating_min" placeholder="Min" />&nbsp;
                              <input type="text" id="rating_max" placeholder="Max" />
                            </td>
                            <td>
                              <input type="text" id="score_min" placeholder="Min" />&nbsp;
                              <input type="text" id="score_max" placeholder="Max" />
                            </td>
                          </tr>
                        </table>

                        <a href="#" id="update_results" class="btn btn-primary m-t-0 pull-right">Update Results</a>
                        <a href="#" id="clear_filters" class="btn btn-default m-t-0 pull-right">Clear Filters</a>

                        <h1 id="title" class="pull-left"></h1>

                        <br>
                        <div class="text-center">
                          <img src="loading.gif" id="loading" /> 
                        </div>

                        <div id="table_wrapper"></div>

                        <br>
                        <p class="text-left">Note: Numbers are based on top 30 products in each sub-category. Products are gathered in real-time. Averages are updated weekly. <span class="pull-right"><strong><a href="#" data-toggle="modal" data-target="#ranking_explained">Ranking Explained</a></strong></span></p>
                      </div>
                        
                    </div>
                    <div class="col-xs-1">
                      <a href="#account_page" data-target="#account_page" class="btn btn-fab btn-material-grey-200 opensource">
                        <img src="user.png" style="width:100%" />
                      </a>
                    </div>
                  </div>

            </div>
          </div>
        </div>

        <!--Modals-->
        <div id="ranking_explained" class="modal fade" tabindex="-1">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Ranking Explained</h3>
              </div>
              <div class="modal-body">
                <p><strong>Avg. BSR</strong>: Average BSR (Best Seller Rank) of products within given sub-category. Ranking is based on comparison between given sub-category and all other sub-categories within their MAJOR (parent) category. <strong>The lower the Avg. BSR, the better (i.e. bigger orange bar)!</strong></p>
                <p><strong>Est. Sales</strong>: Average Estimated Sales <strong>per month</strong> within given sub-category. Calculated using tracked sales data from each MAJOR category and the following logarithmic formula: A * log(B*x); where <strong>A</strong> & <strong>B</strong> are derived constants and <strong>x</strong> is the Avg. BSR of given sub-category. Ranking is based on comparison between given sub-category and all other sub-categories within their MAJOR (parent) category. <strong>The higher the Est. Sales, the better!</strong></p>
                <p><strong># Products</strong>: Number of products within given sub-category. Ranking is based on comparison between given sub-category and all other sub-categories within their MAJOR (parent) category. <strong>The lower the number of products, the better (i.e. bigger orange bar)!</strong></p>
                <p><strong>Avg. Price</strong>: Average Price of products within given sub-category. Ranking is independant to each sub-category. Avg. Price < $20 gets a full score. Avg. Price > $100 gets no score! Avg. Prices between $20 and $100 are ranked in a linear fashion. <strong>The lower the Avg. Price, the better (i.e. bigger orange bar)!</strong></p>
                <p><strong>Reviews</strong>: Average number of Product Reviews within given sub-category. Ranking is based on comparison between given sub-category and all other sub-categories within their MAJOR (parent) category. <strong>The lower the Avg. Reviews, the better (i.e. bigger orange bar)!</strong></p>
                <p><strong>Rating</strong>: Average Product Rating within given sub-category. Ranking is based on comparison between given sub-category and all other sub-categories within their MAJOR (parent) category. <strong>The lower the Avg. Rating, the better (i.e. bigger orange bar)!</strong></p>
                <p><strong>Score</strong>: Cumulative score of given sub-category. Calculated using the above criteria and their correspondig weights. Possible score ranges from 0 (Poor) to 10 (Best). <strong>The higher the Score, the better!</strong></p>
                <hr>
                <p><strong>TL;DR: Full orange bar = Good!</strong> Empty orange bar = Not so good!</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Dismiss</button>
              </div>
            </div>
          </div>
        </div>


        <div id="view_products" class="modal fade" tabindex="-1">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 class="modal-title">Top 10 Products in <strong><span id="subcat_name"></span></strong></h3>
              </div>
              <div class="modal-body">
                Gathering Product List...
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <div id="take_screenshot" class="modal fade" tabindex="-1">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-body">
                
              </div>
              <div class="modal-footer">
              	<button type="button" class="btn btn-primary" id="toggle_names">Toggle Names</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>

        <!-- Open source code -->

        <script type="text/javascript">
            var ajax_requests = new Array();
        </script>

        <?php

          if($_GET['ref'] == 'payment_complete') {

            echo '<script>
              var txn_id = "' . $_GET['tx'] . '";
              $(document).ready(function() {
                
                setTimeout(function() {

                  $("#account_page").html("<h3 id=\'title\' class=\'pull-left\'>One moment while we process your payment...</h3>" +
                        "<br><br>" +
                        "<div class=\'text-center\'>" +
                          "<img src=\'loading.gif\' id=\'loading\' style=\'display: block;\' />" + 
                        "</div>");

                  var page = $("#account_page");

                page.show();

                var totop = setInterval(function () {
                  $(".pages").animate({scrollTop: 0}, 0);
                }, 1);

                setTimeout(function () {
                  page.addClass("active");
                  setTimeout(function () {
                    clearInterval(totop);
                  }, 1000);
                }, 100);

                }, 1000);

                process_payment();
              });
            </script>';
          
          }

        ?>

        <script>
          function process_payment() {

            $.ajax({
                url: "ajax.php",
                type: "POST",
                data: {
                  action: 'process_payment',
                  txn_id: txn_id
                }
            }).done(function(data) { alert(data);

              if(data == 'success') {

                window.location.href = "http://amznichefinder.com/?ref=payment_success#account_page";
              
              } else {

                alert('An error occured. Please refresh the page to try again.');
              }
            });
          }
        </script>

        <script>
          window.page = window.location.hash || "#about";

          $(document).ready(function () {
            var page = $(window.page);

            page.show();

            var totop = setInterval(function () {
              $(".pages").animate({scrollTop: 0}, 0);
            }, 1);

            setTimeout(function () {
              page.addClass("active");
              setTimeout(function () {
                clearInterval(totop);
              }, 1000);
            }, 100);
          });

          $(window).on("resize", function () {
            $("html, body").height($(window).height());
            $(".main, .menu").height($(window).height() - $(".header-panel").outerHeight());
            $(".pages").height($(window).height());
          }).trigger("resize");

          $('a').click(function() {

            window.page = $(this).data("target");

            if(window.page != undefined) {

              if(window.page != '#ranking_explained') {
                $(".menu li").not($(this)).removeClass("active");
                $(".page").removeClass("active").hide();

                window.page = $(this).data("target");

                window.location.hash = window.page;
              }

              var page = $(window.page);


              page.show();

              var totop = setInterval(function () {
                $(".pages").animate({scrollTop: 0}, 0);
              }, 1);

              setTimeout(function () {
                page.addClass("active");
                setTimeout(function () {
                  clearInterval(totop);
                }, 1000);
              }, 100);
            }
          });

          // $('#account, .account_page').click(function() {
          //   $(".menu li").not($(this)).removeClass("active");
          //   $(".page").removeClass("active").hide();

          //   window.page = $(this).data("target");
          //   window.location.hash = window.page;

          //   var page = $(window.page);


          //   page.show();

          //   var totop = setInterval(function () {
          //     $(".pages").animate({scrollTop: 0}, 0);
          //   }, 1);

          //   setTimeout(function () {
          //     page.addClass("active");
          //     setTimeout(function () {
          //       clearInterval(totop);
          //     }, 1000);
          //   }, 100);
          // });

          // $('.reset_page').click(function() {

          //   $(".menu li").not($(this)).removeClass("active");
          //   $(".page").removeClass("active").hide();

          //   window.page = '#reset';

          //   var page = $(window.page);

          //   page.show();

          //   var totop = setInterval(function () {
          //     $(".pages").animate({scrollTop: 0}, 0);
          //   }, 1);

          //   setTimeout(function () {
          //     page.addClass("active");
          //     setTimeout(function () {
          //       clearInterval(totop);
          //     }, 1000);
          //   }, 100);

          // });

          $('#update_results').click(function() {

            getResults();

            return false;

          });

          $('#clear_filters').click(function() {

            $('.filter_options input').val('');

            getResults();

            return false;

          });

          $(".menu li").click(function () {
            // Menu
            if (!$(this).data("target")) return;
            if ($(this).is(".active")) return;
            $(".menu li").not($(this)).removeClass("active");
            $(".page").not(page).removeClass("active").hide();
            window.page = $(this).data("target");
            var page = $(window.page);
            //window.location.hash = window.page;
            $(this).addClass("active");

            page.show();

            var totop = setInterval(function () {
              $(".pages").animate({scrollTop: 0}, 0);
            }, 1);

            setTimeout(function () {
              page.addClass("active");
              setTimeout(function () {
                clearInterval(totop);
              }, 1000);
            }, 100);


            var title = $(this).text();
            $('.page#table_view h1#title').text(title);

            getResults();

            //show products

            $('body').on('click', '.show_products', function() {

              $('#view_products .modal-body').html('Gathering Product List...');

              var cat_id = $(this).attr('id');
              var subcat_name = $(this).closest('td').find('a:first').text();
              $('#view_products #subcat_name').text(subcat_name);

              $('#view_products').modal('show');

              $('#view_products #loading').show();

              for(var i = 0; i < ajax_requests.length; i++) {

                  ajax_requests[i].abort();
              }

              ajax_requests = [];

              ajax_requests.push(

                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {
                      action: 'load_products',
                      cat_id: cat_id
                    }
                }).done(function(data) {

                  $('#view_products #loading').hide();

                  $('#view_products .modal-body').html(data);

                  var asins = $('#view_products .modal-body #asins').val();

                  for(var i = 0; i < ajax_requests.length; i++) {

                      ajax_requests[i].abort();
                  }

                  ajax_requests = [];

                  ajax_requests.push(

                    $.ajax({
                        url: "ajax.php",
                        type: "POST",
                        data: {
                          action: 'load_product_reviews_ratings',
                          asins: asins
                        }
                    }).done(function(data) {

                      var items = data.split(";");

                      for(i in items) {

                        var item = items[i].split(",");

                        $('#view_products #' + item[0] + ' .review_val').html(item[1]);
                        $('#view_products #' + item[0] + ' .rating_val').html(item[2]);
                      }
                    })
                    
                  );

                })

              );

            });
          });


		  $('body').on('click', '.take_screenshot', function() {

              $('#take_screenshot .modal-body').html('One moment...');

              var cat_id = $(this).attr('id');

              var subcat_name = $(this).closest('td').find('a:first').text();
              var subcat_path = $(this).closest('td').find('.subcat_path').html();

              var th_clone = $(this).closest('table').find('tr').eq(0).html();
              var tr_clone = $(this).closest('tr').html();

              $('#take_screenshot').modal('show');

              $('#take_screenshot .modal-body').html('');

              $('#take_screenshot .modal-body').append('<h3><strong>' + subcat_name + '</strong></h3>');
              $('#take_screenshot .modal-body').append(subcat_path + '<br><br>');

              $('#take_screenshot .modal-body').append('<table class="table table-bordered" id="subcats"><tr>' + th_clone + '</tr><tr>' + tr_clone + '</tr></table>');

              $('#take_screenshot #subcats tr td:first, #take_screenshot #subcats tr th:first').hide();

              for(var i = 0; i < ajax_requests.length; i++) {

                  ajax_requests[i].abort();
              }

              ajax_requests = [];

              ajax_requests.push(

                $.ajax({
                    url: "ajax.php",
                    type: "POST",
                    data: {
                      action: 'load_products_table',
                      cat_id: cat_id
                    }
                }).done(function(data) {

                  $('#take_screenshot #loading').hide();

                  $('#take_screenshot .modal-body').append('<h3>Top 10 Products</h3>');
                  $('#take_screenshot .modal-body').append(data);
                  //$('#take_screenshot .modal-body #product_table tbody').html(data);

                  var main_cat_id = $('.menu li.active').attr('id');
                  var asins = $('#take_screenshot .modal-body #asins').val();
                  var bsrs = $('#take_screenshot .modal-body #bsrs').val();

                  for(var i = 0; i < ajax_requests.length; i++) {

                      ajax_requests[i].abort();
                  }

                  ajax_requests = [];

                  ajax_requests.push(

                    $.ajax({
                        url: "ajax.php",
                        type: "POST",
                        data: {
                          action: 'load_product_reviews_ratings',
                          asins: asins
                        }
                    }).done(function(data) {

                      var items = data.split(";");

                      for(i in items) {

                        var item = items[i].split(",");

                        $('#take_screenshot #' + item[0] + ' .review_val').html(item[1]);
                        $('#take_screenshot #' + item[0] + ' .rating_val').html(item[2]);
                      }
                    })
                    
                  );

                  ajax_requests.push(

                    $.ajax({
                        url: "ajax.php",
                        type: "POST",
                        data: {
                          action: 'load_product_est_sales',
                          main_cat_id: main_cat_id,
                          bsrs: bsrs,
                          asins: asins
                        }
                    }).done(function(data) {

                      var items = data.split(";");

                      for(i in items) {

                        var item = items[i].split(",");

                        var price = $('#take_screenshot #' + item[0] + ' td:nth-child(3)').text();

                        $('#take_screenshot #' + item[0] + ' .est_sales_val').html(item[1]);

                        if(price != 'N/A') {
                        	price = parseFloat(price.replace('$', ''));

                        	if(item[1] != '< 5' && item[1] != 'N/A') {
	                        	var revenue = (price * parseInt(item[1])).toLocaleString();
	                        	$('#take_screenshot #' + item[0] + ' .est_rev_val').html('$' + revenue);
	                        
	                        } else {

	                        	$('#take_screenshot #' + item[0] + ' .est_rev_val').html('N/A');
	                        }
                        
                        } else {

                        	$('#take_screenshot #' + item[0] + ' .est_rev_val').html('N/A');
                        }

                        

                      }
                    })
                    
                  );

                })

              );

            });


			$('#toggle_names').click(function() {

				$('#take_screenshot #products tr th:nth-child(2), #take_screenshot #products tr td:nth-child(2)').toggle();
				
				return false;
			});


          function getResults() {

            var cat_id = $(".menu li.active").attr('id');

            var bsr_min = $('#bsr_min').val();
            var bsr_max = $('#bsr_max').val();
            var sales_min = $('#sales_min').val();
            var sales_max = $('#sales_max').val();
            var products_min = $('#products_min').val();
            var products_max = $('#products_max').val();
            var price_min = $('#price_min').val();
            var price_max = $('#price_max').val();
            var reviews_min = $('#reviews_min').val();
            var reviews_max = $('#reviews_max').val();
            var rating_min = $('#rating_min').val();
            var rating_max = $('#rating_max').val();
            var score_min = $('#score_min').val();
            var score_max = $('#score_max').val();

            $('#table_view .dataTables_wrapper').remove();
            $('.upgrade_notice').remove();

            $('#loading').fadeIn('fast');

            for(var i = 0; i < ajax_requests.length; i++) {

                ajax_requests[i].abort();
            }

            ajax_requests = [];

            ajax_requests.push(

              $.ajax({
                  url: "ajax.php",
                  type: "POST",
                  data: {
                    action: 'load_table',
                    cat_id: cat_id,
                    bsr_min: bsr_min,
                    bsr_max: bsr_max,
                    sales_min: sales_min,
                    sales_max: sales_max,
                    products_min: products_min,
                    products_max: products_max,
                    price_min: price_min,
                    price_max: price_max,
                    reviews_min: reviews_min,
                    reviews_max: reviews_max,
                    rating_min: rating_min,
                    rating_max: rating_max,
                    score_min: score_min,
                    score_max: score_max
                  }
              }).done(function(data) { console.log(data);

                $('#loading').hide();

                $('#table_view #table_wrapper').html(data);

                var membership = $('#table_view #membership').val();

                if(membership == 1) {

                  var sort = 'asc';
                
                } else {

                  var sort = 'desc';
                }

                $('#table_view table#subcats').DataTable({
                    "order": [[ 7, sort ]]
                });
              })
            );

          }

          // function cleanSource(html) {
          //   var lines = html.split(/\n/);

          //   lines.shift();
          //   lines.splice(-1, 1);

          //   var indentSize = lines[0].length - lines[0].trim().length,
          //       re = new RegExp(" {" + indentSize + "}");

          //   lines = lines.map(function (line) {
          //     if (line.match(re)) {
          //       line = line.substring(indentSize);
          //     }

          //     return line;
          //   });

          //   lines = lines.join("\n");

          //   return lines;
          // }

          // $("#opensource").click(function () {
          //   $.get(window.location.href, function (data) {
          //     var html = $(data).find(window.page).html();
          //     html = cleanSource(html);
          //     $("#source-modal pre").text(html);
          //     $("#source-modal").modal();
          //   });
          // });
        </script>

        <!-- Twitter Bootstrap -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js"></script>

        <!-- Material Design for Bootstrap -->
        <script src="js/material.js"></script>
        <script src="js/ripples.min.js"></script>
        <script>
          $.material.init();
        </script>


        <!-- Sliders -->
        <!--<script src="//cdnjs.cloudflare.com/ajax/libs/noUiSlider/6.2.0/jquery.nouislider.min.js"></script>-->
        <script>
          // $(function () {
          //   $.material.init();
          //   $(".shor").noUiSlider({
          //     start: 40,
          //     connect: "lower",
          //     range: {
          //       min: 0,
          //       max: 100
          //     }
          //   });

          //   $(".svert").noUiSlider({
          //     orientation: "vertical",
          //     start: 40,
          //     connect: "lower",
          //     range: {
          //       min: 0,
          //       max: 100
          //     }
          //   });
          // });
        </script>

        <!-- Dropdown.js -->
        <script src="https://cdn.rawgit.com/FezVrasta/dropdown.js/master/jquery.dropdown.js"></script>
        <script>
          $("#dropdown-menu select").dropdown();
        </script>

        <!-- Data Tables -->
        <script src="http://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            jQuery.extend( jQuery.fn.dataTableExt.oSort, {
              "numeric-comma-pre": function ( a ) {
                var x = (a == "-") ? 0 : a.replace( /,/, "." );
                return parseFloat( x );
              },

              "numeric-comma-asc": function ( a, b ) {
                return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },

              "numeric-comma-desc": function ( a, b ) {
                return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
            } );

            $(document).ready(function(){
                $('#table_view table#subcats').DataTable({
                    "order": [[ 7, "asc" ]],
                    "columnDefs": [
                        { "type": "numeric-comma", targets: 1 }
                    ]
                });
            });
        </script>

    </body>
</html>
