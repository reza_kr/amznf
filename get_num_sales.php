<?php

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$proxies = file('ppx.txt');

	foreach ($proxies as $key => $proxy) {
		
		$proxies[$key] = array(trim($proxy), 0);
	}


	$main_cats = array(
		"1000" => "Books",
		"1063498" => "Home+%26+Kitchen",
		"1084128" => "Office+Products",
		"11055981" => "Beauty",
		"11965861" => "Musical+Instruments",
		"15690151" => "Automotive",
		"16310161" => "Industrial+%26+Scientific",
		"16310211" => "Grocery+%26+Gourmet+Food",
		"165795011" => "Toys+%26+Games",
		"165797011" => "Baby",
		"2335753011" => "Cell+Phones+%26+Accessories",
		"2617942011" => "Arts%2C+Crafts+%26+Sewing",
		"2619526011" => "Appliances",
		"2619534011" => "Pet+Supplies",
		"3238155011" => "Patio%2C+Lawn+%26+Garden",
		"3375301" => "Sports+%26+Outdoors",
		"3760931" => "Health+%26+Personal+Care",
		"468240" => "Home+Improvement",
		"493964" => "Electronics",
		"541966" => "Computers",
		"7141124011" => "Clothing"
		// "7141124011_shoes" => "Shoes",
		// "7141124011_jewelry" => "Jewelry"
	);


	DEFINE ('DBUSER', 'remote');
	DEFINE ('DBPASS', 'GQ8XEQAVgwvVL8EpTUCaVS6S');
	DEFINE ('DBHOST', '107.170.7.163');
	DEFINE ('DBNAME', 'niche_finder');

    $dbo = new PDO('mysql:host=' .DBHOST. ';dbname=' .DBNAME, DBUSER, DBPASS);

    $query = $dbo->prepare("SELECT id, parent_id, avg_sales_rank FROM sub_cats WHERE avg_sales_rank > ? AND js_num_sales = ?");
	$query->execute(array(0, 0));

	$result = $query->fetchAll();

	foreach($result as $row) {

		$sub_cat_id = $row[0];
		$parent_id = $row[1];
		$avg_sales_rank = $row[2];

		$main_cat = $main_cats[$parent_id];

		$rand_key = array_rand($proxies);

		$url = 'https://junglescoutpro.herokuapp.com/api/v1/est_sales?store=us&rank=' . $avg_sales_rank . '&category=' . $main_cat;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_PROXY, $proxies[$rand_key][0]);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_REFERER, 'http://www.junglescout.com/estimator/');
		//curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds
		
		$result = json_decode(curl_exec($ch), true);
		curl_close($ch);

		$num_sales = $result["estSalesResult"];

		if(trim($num_sales) == "< 5") { $num_sales = 4; }

		$query = $dbo->prepare("UPDATE sub_cats SET js_num_sales = ? WHERE id = ?");
		$query->execute(array($num_sales, $sub_cat_id));

		echo $num_sales . "\r\n";

		sleep(rand(5, 30));
	}

?>
