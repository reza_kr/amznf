<?php

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	require_once('simple_html_dom.php');

	$proxies = file('ppx.txt');

	foreach ($proxies as $key => $proxy) {
		
		$proxies[$key] = array(trim($proxy), 0);
	}

	DEFINE ('DBUSER', 'remote');
	DEFINE ('DBPASS', 'GQ8XEQAVgwvVL8EpTUCaVS6S');
	DEFINE ('DBHOST', '107.170.7.163');
	DEFINE ('DBNAME', 'niche_finder');

    $dbo = new PDO('mysql:host=' .DBHOST. ';dbname=' .DBNAME, DBUSER, DBPASS);
	
	require_once "vendor/autoload.php";

	use ApaiIO\Configuration\GenericConfiguration;
	use ApaiIO\Operations\Search;
	use ApaiIO\Operations\BrowseNodeLookup;
	use ApaiIO\ApaiIO;

	$conf = new GenericConfiguration();

	$conf
	    ->setCountry('com')
	    ->setAccessKey('AKIAJJ3K6XF7GAT5MM6Q')
	    ->setSecretKey('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')
	    ->setAssociateTag('azart-20');

	$apaiIo = new ApaiIO($conf);


	// ==========================================================================
	// ============ Get Sub Category Data ===

	function getSubcatData() {

		global $dbo;
		global $apaiIo;
		global $proxies;


		//get formulas
		$formulas = array();

	    $query = $dbo->prepare("SELECT * FROM main_cats");
		$query->execute(array());

		$result = $query->fetchAll();

		foreach($result as $row) {

			$cat = array();

			$cat['name'] = $row[1];
			$cat['A'] = $row[2];
			$cat['B'] = $row[3];
			$cat['C'] = $row[4];
			$cat['D'] = $row[5];

			$formulas[$row[0]] = $cat;
		}

		//start scan...
		$query = $dbo->prepare("SELECT id, name, parent_id FROM sub_cats WHERE group_id = ? ORDER BY update_counter ASC");
		$query->execute(array(1));

		$result = $query->fetchAll();

		foreach($result as $row) {

			$sub_cat_id = $row[0];
			$cat_name = $row[1];
			$parent_id = $row[2];

			$total_sales_rank = 0;
			$total_price = 0;
			$total_reviews = 0;
			$total_rating = 0;

			$no_price_count = 0;
			$no_review_rating = 0;
			$no_sales_rank = 0;

			$num_products = 0;
			$num_pages = 3; //default is 10, overwritten in the loop, once num_products are known.
			
			$page = 1;

			echo $cat_name . "\r\n";

			while($page <= $num_pages) { 

				//clean up proxy list
				$proxy_string = "";

				$bad_proxy_found = false;

				foreach ($proxies as $key => $proxy) {
					
					if($proxy[1] >= 100000000) { //proxy has failed 5 times...

						unset($proxies[$key]);

						$bad_proxy_found = true;

						echo "Bad Proxy: " . $proxy[0] . "\r\n";
					
					} else {

						$proxy_string .= $proxy[0] . "\r\n";
					}
				}

				if($bad_proxy_found == true) {

					//$proxy_string = substr($proxy_string, 0, -4);

					file_put_contents('ppx.txt', $proxy_string);

					echo "Proxy List Updated...\r\n";

					$bad_proxy_found = false;

					if(count($proxies) == 0) {

						echo "No more proxies...\r\n";
						exit;
					}
				}


				$search = new Search();
				$search->setCategory('Baby');
				$search->setBrowseNode($sub_cat_id);
				$search->setResponseGroup(array('SalesRank', 'Reviews', 'Offers'));
				$search->setPage($page);
				$search->setKeywords('-dsfdffddfdsf');


				$failed = false;
				$too_quick = false;

				do {

				    try {
				        
				        $response = $apaiIo->runOperation($search);

				        $failed = false;

				        $xml = simplexml_load_string($response);

				        if(isset($xml->Error)) {

				        	$too_quick = true;

				        	echo "Too quick...\r\n";
				        
				        } else {

				        	$too_quick = false;
				        }
				    
				    } catch (Exception $e) {
				        
				        sleep(1);

				        $failed = true;

				        continue;
				    }

				} while($failed == true || $too_quick == true);

				$xml = simplexml_load_string($response);
				
				if($page == 1) {

					$num_products = (int)$xml->Items->TotalResults; //////////

					if($num_products <= 30) {

						$num_pages = ceil($num_products / 10);
					}

					echo "# Products: " . $num_products . "\r\n";
				}

				echo "Page " . $page . " of " . $num_pages . "\r\n";

				foreach ($xml->Items->Item as $item) {

					//usleep(500000); // sleep half a second
					
					$asin = (string)$item->ASIN;

					if(isset($item->SalesRank)) {

						$sales_rank = (string)$item->SalesRank;

						$total_sales_rank += $sales_rank;

					} else {

						$no_sales_rank++;

						echo "No sales rank... " . $asin . "\r\n";
					}
					
					//print_r($item); exit;
					if(isset($item->Offers->Offer[0]->OfferListing->Price->Amount)) {

						$price = ((int)$item->Offers->Offer[0]->OfferListing->Price->Amount) / 100; 

						$total_price += $price; ///////////
					
					} else {

						$no_price_count++;

						echo "No price... " . $asin . "\r\n";
					}
					
					//add to tracked products
					$query = $dbo->prepare("INSERT IGNORE INTO tracked_products SET asin = ?, main_cat_id = ?, sub_cat_id = ?");
					$query->execute(array($asin, $parent_id, $sub_cat_id));


					$review_url = 'http://www.amazon.com/gp/customer-reviews/widgets/average-customer-review/popover/ref=dpx_acr_pop_?contextId=dpx&asin=' . $asin;

					$html = "";
					$tries = 0;

					do {

						$fetch_failed = false;

						$rand_key = array_rand($proxies);

						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL, $review_url);
						curl_setopt($ch, CURLOPT_PROXY, $proxies[$rand_key][0]);
						curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_HEADER, 1);
						curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
						curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds
						$curl_scraped_page = curl_exec($ch);
						curl_close($ch);

						$html = str_get_html($curl_scraped_page);

						if(is_object($html)) {

							//$html = file_get_html($review_url);

							if(!($html->find('span[class=a-size-base a-color-secondary]', 0))) {

								$fetch_failed = true;

								echo "Fetching Reviews faileddd... " . $proxies[$rand_key][0] . "\r\n";

								$proxies[$rand_key][1] += 1;

								//echo $curl_scraped_page; exit;
							}

						} else {

							$fetch_failed = true;

							echo "Fetching Reviews failed... " . $proxies[$rand_key][0] . "\r\n";

							$proxies[$rand_key][1] += 1;

							//echo $curl_scraped_page; exit;
						}

						$tries++;
					
					} while($fetch_failed == true);
					//} while($fetch_failed == true && $tries <= 10);

					if(!$fetch_failed) { //success

						echo "Reviews fetched!... " . $proxies[$rand_key][0] . "\r\n";
						
						$rating = $html->find('span[class=a-size-base a-color-secondary]', 0)->plaintext;
						$rating = trim($rating);
						$rating = (float)substr($rating, 0, strpos($rating, ' out of'));
						
						$num_reviews = $html->find('a[class=a-size-small a-link-emphasis]', 0)->plaintext;
						$num_reviews = trim($num_reviews);
						$num_reviews = substr($num_reviews, strpos($num_reviews, 'See all ') + 8);
						$num_reviews = intval(str_replace(",", "", substr($num_reviews, 0, strpos($num_reviews, ' reviews'))));

						$total_reviews += $num_reviews;

						if($rating <= 0) {

							$total_rating += 2.5; //if no rating, set to 2.5 to minimize effect on avg. calculation.
						
						} else {

							$total_rating += $rating;
						}
					
					} else { //fetch failed

						$no_review_rating++;
						echo "No review/rating... " . $asin . "\r\n";
					}

					//$total_price += $price; //moved up
					
					
				    //exit;
				}

				$page++;

			}

			if($num_products < 30) {

				$div_by = $num_products; //use all since less than 100
			
			} else {

				$div_by = 30; //only scanning the top 100 products...

			}

			$avg_sales_rank = $total_sales_rank / ($div_by - $no_sales_rank); //////////
			$avg_price = $total_price / ($div_by - $no_price_count); ////////// exclude the items that did not have a price...
			$avg_reviews = round($total_reviews / ($div_by - $no_review_rating)); //////////
			$avg_rating = $total_rating / ($div_by - $no_review_rating); //////////

			if($avg_sales_rank > 0) {

				//$num_sales = (float)$formulas[$parent_id]['A'] * (log10(abs((float)$formulas[$parent_id]['B'] * ($avg_sales_rank + (int)$formulas[$parent_id]['C'])))) + (int)$formulas[$parent_id]['D'];
				
				//do not calculate number of sales -- will use JungleScout to gather

			} else {

				//$num_sales = 0;
			}

			echo 'Avg BSR: ' . $avg_sales_rank . '
Avg Price: $' . $avg_price . '
Avg Rating: ' . $avg_rating . '
Avg Reviews: ' . $avg_reviews . "\r\n\r\n";

			$datetime = date('Y-m-d H:i:s');

			if($avg_sales_rank > 0) {

				$query = $dbo->prepare("UPDATE sub_cats SET avg_sales_rank = ?, num_products = ?, avg_price = ?, avg_reviews = ?, avg_rating = ?, last_updated = ?, update_counter = update_counter + 1 WHERE id = ?");
				$query->execute(array($avg_sales_rank, $num_products, $avg_price, $avg_reviews, $avg_rating, $datetime, $sub_cat_id));
			}

			//exit;
		}
	}

	getSubcatData();

?>