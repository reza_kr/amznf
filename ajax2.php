<?php

    session_start();

    //error_reporting(E_ALL);
    //ini_set('display_errors', 'On');

    require_once "vendor/autoload.php";

    use ApaiIO\Configuration\GenericConfiguration;
    use ApaiIO\Operations\Search;
    use ApaiIO\Operations\BrowseNodeLookup;
    use ApaiIO\ApaiIO;

    $affiliate_id = 'amznf-20';

    if($_POST['action'] == 'load_table') {

        $cat_id = $_POST['cat_id'];

        DEFINE ('DBUSER', 'remote');
        DEFINE ('DBPASS', 'GQ8XEQAVgwvVL8EpTUCaVS6S');
        DEFINE ('DBHOST', '107.170.7.163');
        DEFINE ('DBNAME', 'niche_finder');

        $dbo = new PDO('mysql:host=' .DBHOST. ';dbname=' .DBNAME, DBUSER, DBPASS);


        // $query = $dbo->prepare("SELECT min(avg_sales_rank), max(avg_sales_rank) FROM sub_cats WHERE last_updated <> ? AND parent_id = ?");
        // $query->execute(array('0000-00-00 00:00:00', $cat_id));

        // $result = $query->fetchAll();

        // foreach($result as $row) {

        //     $min_sales_rank = $row[0];
        //     $max_sales_rank = $row[1];
        // }


        $query = $dbo->prepare("SELECT min(avg_sales_rank), max(avg_sales_rank), min(num_sales), max(num_sales), min(num_products), max(num_products), min(avg_price), max(avg_price), min(avg_reviews), max(avg_reviews), min(avg_rating), max(avg_rating) FROM sub_cats WHERE last_updated <> ? AND parent_id = ?");
        $query->execute(array('0000-00-00 00:00:00', $cat_id));

        $result = $query->fetchAll();

        foreach($result as $row) {

            $min_sales_rank = $row[0];
            $max_sales_rank = $row[1];
            $min_sales = $row[2];
            $max_sales = $row[3];
            $min_products = $row[4];
            $max_products = $row[5];
            $min_price = $row[6];
            $max_price = $row[7];
            $min_reviews = $row[8];
            $max_reviews = $row[9];
            $min_rating = $row[10];
            $max_rating = $row[11];
        }

        $sales_rank_weight = 2 / ($max_sales_rank - $min_sales_rank);
        $sales_weight = 3 / (($max_sales + (0 - $min_sales)) - ($min_sales + (0 - $min_sales)));
        $products_weight = 2 / ($max_products - $min_products);
        $price_weight = 1 / (100 - 20);
        $reviews_weight = 1 / ($max_reviews - $min_reviews);
        $rating_weight = 1 / ($max_rating - $min_rating);

        //////////////////////////

        $query_arr = array('0000-00-00 00:00:00', $cat_id);

        $query = 'SELECT id, name, cat_path, avg_sales_rank, num_sales, num_products, avg_price, avg_reviews, avg_rating FROM sub_cats WHERE last_updated <> ? AND parent_id = ?';

        $sales_min = ($_POST['sales_min'] <= 5) ? -100 : ($_POST['sales_min'] / 30);
        $sales_max = (is_numeric($_POST['sales_max'])) ? ($_POST['sales_max'] / 30) : '-';

        $table .= $sales_min . ', ' . $sales_max;

        if(is_numeric($_POST['bsr_min'])) {$query .= ' AND avg_sales_rank >= ?'; array_push($query_arr, $_POST['bsr_min']);}
        if(is_numeric($_POST['bsr_max'])) {$query .= ' AND avg_sales_rank <= ?'; array_push($query_arr, $_POST['bsr_max']);}
        if(is_numeric($sales_min)) {$query .= ' AND num_sales >= ?'; array_push($query_arr, $sales_min);}
        if(is_numeric($sales_max)) {$query .= ' AND num_sales <= ?'; array_push($query_arr, $sales_max);}
        if(is_numeric($_POST['products_min'])) {$query .= ' AND num_products >= ?'; array_push($query_arr, $_POST['products_min']);}
        if(is_numeric($_POST['products_max'])) {$query .= ' AND num_products <= ?'; array_push($query_arr, $_POST['products_max']);}
        if(is_numeric($_POST['price_min'])) {$query .= ' AND avg_price >= ?'; array_push($query_arr, $_POST['price_min']);}
        if(is_numeric($_POST['price_max'])) {$query .= ' AND avg_price <= ?'; array_push($query_arr, $_POST['price_max']);}
        if(is_numeric($_POST['reviews_min'])) {$query .= ' AND avg_reviews >= ?'; array_push($query_arr, $_POST['reviews_min']);}
        if(is_numeric($_POST['reviews_max'])) {$query .= ' AND avg_reviews <= ?'; array_push($query_arr, $_POST['reviews_max']);}
        if(is_numeric($_POST['rating_min'])) {$query .= ' AND avg_rating >= ?'; array_push($query_arr, $_POST['rating_min']);}
        if(is_numeric($_POST['rating_max'])) {$query .= ' AND avg_rating <= ?'; array_push($query_arr, $_POST['rating_max']);}

        $query = $dbo->prepare($query);
        $query->execute($query_arr);

        $result = $query->fetchAll();

        foreach($result as $row) {

            $id = $row[0];
            $name = $row[1];
            $cat_path = $row[2];
            $avg_sales_rank = $row[3];
            $num_sales = $row[4];
            $num_products = $row[5];
            $avg_price = $row[6];
            $avg_reviews = $row[7];
            $avg_rating = $row[8];

            if($num_products > 0) {

                if($num_sales <= 0.1666666666) {

                    $sales_score = 0;

                    $monthly_sales = "5";

                    $less_than_5 = "less_than_5";
                
                } else {

                    $sales_score = round((($num_sales - $min_sales) * $sales_weight), 5);

                    $monthly_sales = intval(round($num_sales * 30));

                    $less_than_5 = "";
                }

                $sales_rank_score = round((2 - (($avg_sales_rank - $min_sales_rank) * $sales_rank_weight)), 5);
                $products_score = round((2 - (($num_products - $min_products) * $products_weight)), 5);
                $price_score = round(($avg_price < 20 ? '1.0000000000' : ($avg_price > 100 ? '0.0000000000' : (1 - (($avg_price - 20) * $price_weight)))), 5);
                $reviews_score = round((1 - (($avg_reviews - $min_reviews) * $reviews_weight)), 5);
                $rating_score = round((1 - (($avg_rating - $min_rating) * $rating_weight)), 5);

                //$table .= '<tr><td colspan="8">' . $sales_rank_score . ', ' . $sales_score . ', ' . $products_score . ', ' . $price_score . ', ' . $reviews_score . ', ' . $rating_score . '</td></tr>';

                $total_score = $sales_rank_score + $sales_score + $products_score + $price_score + $reviews_score + $rating_score;

            } else {

                $sales_rank_score = 0;
                $sales_score = 0;
                $products_score = 0;
                $price_score = 0;
                $reviews_score = 0;
                $rating_score = 0;

                $total_score = 0;
            }

            $score_min_met = false;
            $score_max_met = false;

            if(is_numeric($_POST['score_min'])) {

                if($_POST['score_min'] <= $total_score) {

                    $score_min_met = true;
                }
            
            } else {

                $score_min_met = true;
            }

            if(is_numeric($_POST['score_max'])) {

                if($_POST['score_max'] >= $total_score) {

                    $score_max_met = true;
                }
            
            } else {

                $score_max_met = true;
            }


            if($score_min_met && $score_max_met) {

                $cat_path = json_decode($cat_path, true);

                $cat_path_string = '';

                foreach ($cat_path as $key => $node) {
                     
                    if($total_score > 7 && !$_SESSION['logged_in']) {

                        $len = strlen($node[1]);

                        $cat_path_string .= '<span class="blurry">' . str_repeat('-', $len) . '</span> > ';

                    } else {

                        $cat_path_string .= '<a target="_blank" href="http://www.amazon.com/b/ref=dp_bc_4?ie=UTF8&node=' . $node[0] . '&tag=' . $affiliate_id . '" onclick="ga(\'send\', \'event\', \'CatPath\', \'Path\', \'' . $node[1] . '\');">' . $node[1] . '</a> > ';
                    }
                }

                $cat_path_string = '<span class="cat_path">' . substr($cat_path_string, 0, -3) . '</span>';

                if(round($total_score, 2) > 7 && !$_SESSION['logged_in']) {

                    $name_parts = explode(' ', $name);
                    $name = '';

                    foreach ($name_parts as $part) {

                        $len = strlen($part);
                        $name .= str_repeat('-', $len) . ' ';
                    }

                    $name = substr($name, 0, -1);

                    $name_column = '<span class="blurry">' . $name . '</span><br>
                                    ' . $cat_path_string;
                
                } else {

                    $name_column = '<a class="cat_name" target="_blank" href="http://www.amazon.com/b/ref=dp_bc_4?ie=UTF8&node=' . $id . '&tag=' . $affiliate_id . '" onclick="ga(\'send\', \'event\', \'SubCat\', \'Title\', \'' . $name . '\');">' . $name . '</a> (<a href="#" id="' . $id . '" class="show_products">Show Products</a>)<br>
                                    ' . $cat_path_string;
                }


                $table .= '<tr>
                            <td>
                                ' . $name_column . '
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . (($sales_rank_score / 2) * 100) . '%"></div>
                                </div>
                                <span>' . number_format($avg_sales_rank) . '</span>
                            </td>
                            <td class="' . $less_than_5 . '">
                                <div class="bar">
                                    <div class="fill" style="width:' . (($sales_score / 3) * 100) . '%"></div>
                                </div>
                                <span>' . $monthly_sales . '</span>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . (($products_score / 2) * 100) . '%"></div>
                                    <span>' . number_format($num_products) . '</span>
                                </div>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . ($price_score * 100) . '%"></div>
                                    <span>$' . number_format($avg_price, 2) . '</span>
                                </div>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . ($reviews_score * 100) . '%"></div>
                                    <span>' . $avg_reviews . '</span>
                                </div>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . ($rating_score * 100) . '%"></div>
                                    <span>' . round($avg_rating, 1) . '
                                </div>
                            </td>
                            <td>
                                <span>' . round($total_score, 2) . '</span>
                            </td>
                        </tr>';
            }
        }

        $table = '<table class="table table-bordered table-striped table-hover" id="subcats" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Avg. BSR</th>
                            <th>Est. Sales</th>
                            <th># Products</th>
                            <th>Avg. Price</th>
                            <th>Avg. Reviews</th>
                            <th>Avg. Rating</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>' .

                    $table .

                    '</tbody>
                </table>';

        echo $table;

    }


?>