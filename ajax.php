<?php

    session_start();

    //error_reporting(E_ALL);
    //ini_set('display_errors', 'On');

    require_once "vendor/autoload.php";

    use ApaiIO\Configuration\GenericConfiguration;
    use ApaiIO\Operations\Search;
    use ApaiIO\Operations\BrowseNodeLookup;
    use ApaiIO\ApaiIO;

    $affiliate_id = 'am0fa-20';

    if($_POST['action'] == 'load_table') {

        $cat_id = $_POST['cat_id'];

        DEFINE ('DBUSER', 'remote');
        DEFINE ('DBPASS', 'GQ8XEQAVgwvVL8EpTUCaVS6S');
        DEFINE ('DBHOST', '107.170.7.163');
        DEFINE ('DBNAME', 'niche_finder');

        $dbo = new PDO('mysql:host=' .DBHOST. ';dbname=' .DBNAME, DBUSER, DBPASS);


        // $query = $dbo->prepare("SELECT min(avg_sales_rank), max(avg_sales_rank) FROM sub_cats WHERE last_updated <> ? AND parent_id = ?");
        // $query->execute(array('0000-00-00 00:00:00', $cat_id));

        // $result = $query->fetchAll();

        // foreach($result as $row) {

        //     $min_sales_rank = $row[0];
        //     $max_sales_rank = $row[1];
        // }


        $query = $dbo->prepare("SELECT min(avg_sales_rank), max(avg_sales_rank), min(js_num_sales), max(js_num_sales), min(num_products), max(num_products), min(avg_price), max(avg_price), min(avg_reviews), max(avg_reviews), min(avg_rating), max(avg_rating) FROM sub_cats WHERE last_updated <> ? AND parent_id = ?");
        $query->execute(array('0000-00-00 00:00:00', $cat_id));

        $result = $query->fetchAll();

        foreach($result as $row) {

            $min_sales_rank = $row[0];
            $max_sales_rank = $row[1];
            $min_sales = $row[2];
            $max_sales = $row[3];
            $min_products = $row[4];
            $max_products = $row[5];
            $min_price = $row[6];
            $max_price = $row[7];
            $min_reviews = $row[8];
            $max_reviews = $row[9];
            $min_rating = $row[10];
            $max_rating = $row[11];
        }

        $sales_rank_weight = 2 / ($max_sales_rank - $min_sales_rank);
        $sales_weight = 3 / (($max_sales + (0 - $min_sales)) - ($min_sales + (0 - $min_sales)));
        $products_weight = 2 / ($max_products - $min_products);
        $price_weight = 1 / (100 - 20);
        $reviews_weight = 1 / ($max_reviews - $min_reviews);
        $rating_weight = 1 / ($max_rating - $min_rating);

        //////////////////////////

        $query_arr = array('0000-00-00 00:00:00', $cat_id, 0);

        $query = 'SELECT id, name, cat_path, avg_sales_rank, js_num_sales, num_products, avg_price, avg_reviews, avg_rating FROM sub_cats WHERE last_updated <> ? AND parent_id = ? AND num_products > ?';

        //$sales_min = ($_POST['sales_min'] <= 5) ? -100 : ($_POST['sales_min'] / 30);
        //$sales_max = (is_numeric($_POST['sales_max'])) ? ($_POST['sales_max'] / 30) : '-';

        $sales_min = ($_POST['sales_min'] <= 5) ? -100 : ($_POST['sales_min']);
        $sales_max = (is_numeric($_POST['sales_max'])) ? ($_POST['sales_max']) : '-';

        if(is_numeric($_POST['bsr_min'])) {$query .= ' AND avg_sales_rank >= ?'; array_push($query_arr, $_POST['bsr_min']);}
        if(is_numeric($_POST['bsr_max'])) {$query .= ' AND avg_sales_rank <= ?'; array_push($query_arr, $_POST['bsr_max']);}
        if(is_numeric($sales_min)) {$query .= ' AND js_num_sales >= ?'; array_push($query_arr, $sales_min);}
        if(is_numeric($sales_max)) {$query .= ' AND js_num_sales <= ?'; array_push($query_arr, $sales_max);}
        if(is_numeric($_POST['products_min'])) {$query .= ' AND num_products >= ?'; array_push($query_arr, $_POST['products_min']);}
        if(is_numeric($_POST['products_max'])) {$query .= ' AND num_products <= ?'; array_push($query_arr, $_POST['products_max']);}
        if(is_numeric($_POST['price_min'])) {$query .= ' AND avg_price >= ?'; array_push($query_arr, $_POST['price_min']);}
        if(is_numeric($_POST['price_max'])) {$query .= ' AND avg_price <= ?'; array_push($query_arr, $_POST['price_max']);}
        if(is_numeric($_POST['reviews_min'])) {$query .= ' AND avg_reviews >= ?'; array_push($query_arr, $_POST['reviews_min']);}
        if(is_numeric($_POST['reviews_max'])) {$query .= ' AND avg_reviews <= ?'; array_push($query_arr, $_POST['reviews_max']);}
        if(is_numeric($_POST['rating_min'])) {$query .= ' AND avg_rating >= ?'; array_push($query_arr, $_POST['rating_min']);}
        if(is_numeric($_POST['rating_max'])) {$query .= ' AND avg_rating <= ?'; array_push($query_arr, $_POST['rating_max']);}

        $query = $dbo->prepare($query);
        $query->execute($query_arr);

        $result = $query->fetchAll();
        
        foreach($result as $row) {
        	
            $id = $row[0];
            $name = $row[1];
            $cat_path = $row[2];
            $avg_sales_rank = $row[3];
            $num_sales = $row[4];
            $num_products = $row[5];
            $avg_price = $row[6];
            $avg_reviews = $row[7];
            $avg_rating = $row[8];

            if($num_products > 0) {

                if($num_sales < 5) {

                    $sales_score = 0;

                    $monthly_sales = "5";

                    $less_than_5 = "less_than_5";
                
                } else {

                    $sales_score = round((($num_sales - $min_sales) * $sales_weight), 5);

                    //$monthly_sales = intval(round($num_sales * 30));
                    $monthly_sales = $num_sales;

                    $less_than_5 = "";
                }

                $sales_rank_score = round((2 - (($avg_sales_rank - $min_sales_rank) * $sales_rank_weight)), 5);
                $products_score = round((2 - (($num_products - $min_products) * $products_weight)), 5);
                $price_score = round(($avg_price < 20 ? '1.0000000000' : ($avg_price > 100 ? '0.0000000000' : (1 - (($avg_price - 20) * $price_weight)))), 5);
                $reviews_score = round((1 - (($avg_reviews - $min_reviews) * $reviews_weight)), 5);
                $rating_score = round((1 - (($avg_rating - $min_rating) * $rating_weight)), 5);

                //$table .= '<tr><td colspan="8">' . $sales_rank_score . ', ' . $sales_score . ', ' . $products_score . ', ' . $price_score . ', ' . $reviews_score . ', ' . $rating_score . '</td></tr>';

                $total_score = $sales_rank_score + $sales_score + $products_score + $price_score + $reviews_score + $rating_score;

            } else {

                $sales_rank_score = 0;
                $sales_score = 0;
                $products_score = 0;
                $price_score = 0;
                $reviews_score = 0;
                $rating_score = 0;

                $total_score = 0;
            }

            $score_min_met = false;
            $score_max_met = false;

            if(is_numeric($_POST['score_min'])) {

                if($_POST['score_min'] <= $total_score) {

                    $score_min_met = true;
                }
            
            } else {

                $score_min_met = true;
            }

            if(is_numeric($_POST['score_max'])) {

                if($_POST['score_max'] >= $total_score) {

                    $score_max_met = true;
                }
            
            } else {

                $score_max_met = true;
            }


            if($score_min_met && $score_max_met) {

                $cat_path = json_decode($cat_path, true);

                $cat_path_string = '';

                $now = time();

				if($_SESSION['membership_status'] == 1 || ($_SESSION['membership_status'] == 0 && strtotime($_SESSION['expiry_date']) > $now)) {

				  	foreach ($cat_path as $key => $node) {

				  		$cat_path_string .= '<a target="_blank" href="http://www.amazon.com/b/ref=dp_bc_4?ie=UTF8&node=' . $node[0] . '&tag=' . $affiliate_id . '" onclick="ga(\'send\', \'event\', \'CatPath\', \'Path\', \'' . $node[1] . '\');">' . $node[1] . '</a> > ';
                     
	                }

	                $name_column = '<a class="cat_name" target="_blank" href="http://www.amazon.com/b/ref=dp_bc_4?ie=UTF8&node=' . $id . '&tag=' . $affiliate_id . '" onclick="ga(\'send\', \'event\', \'SubCat\', \'Title\', \'' . $name . '\');">' . $name . '</a><br>
                                    <span class="subcat_path hidden">' . $cat_path_string . '</span><a href="#" class="take_screenshot" id="' . $id . '"><img src="screenshot.png" /></a>';

				} else {

				  	foreach ($cat_path as $key => $node) {
                     
	                    if($total_score > 7) {

	                        $len = strlen($node[1]);

	                        $cat_path_string .= '<span class="blurry">' . str_repeat('-', $len) . '</span> > ';

	                        ////////////////////////

	                        $name_parts = explode(' ', $name);
		                    $name = '';

		                    foreach ($name_parts as $part) {

		                        $len = strlen($part);
		                        $name .= str_repeat('-', $len) . ' ';
		                    }

		                    $name = substr($name, 0, -1);

		                    $name_column = '<span class="blurry">' . $name . '</span><br>
		                                    ' . $cat_path_string;

		                    $upgrade_notice = '<br><br><p class="alert alert-warning upgrade_notice" style="background: #E39316;"><strong>Note:</strong> As a free user, you are limited to niches with scores between 0-7. To view niches with scores above 7, please sign up for a membership.</p>';


	                    } else {

	                        $cat_path_string .= '<a target="_blank" href="http://www.amazon.com/b/ref=dp_bc_4?ie=UTF8&node=' . $node[0] . '&tag=' . $affiliate_id . '" onclick="ga(\'send\', \'event\', \'CatPath\', \'Path\', \'' . $node[1] . '\');">' . $node[1] . '</a> > ';
	                    
	                        $name_column = '<a class="cat_name" target="_blank" href="http://www.amazon.com/b/ref=dp_bc_4?ie=UTF8&node=' . $id . '&tag=' . $affiliate_id . '" onclick="ga(\'send\', \'event\', \'SubCat\', \'Title\', \'' . $name . '\');">' . $name . '</a><br>
                                    <span class="subcat_path hidden">' . $cat_path_string . '</span><a href="#" class="take_screenshot" id="' . $id . '"><img src="screenshot.png" /></a>';
	                    
	                    	$membership_indicator = '<input type="hidden" id="membership" value="1" />'; //used for sorting by score, asc or desc
	                    }
	                }
				}

                $cat_path_string = '<span class="cat_path">' . substr($cat_path_string, 0, -3) . '</span>';


                $table .= '<tr>
                            <td>
                                ' . $name_column . '
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . (($sales_rank_score / 2) * 100) . '%"></div>
                                </div>
                                <span>' . ($avg_sales_rank > 0 ? number_format($avg_sales_rank) : 'N/A') . '</span>
                            </td>
                            <td class="' . $less_than_5 . '">
                                <div class="bar">
                                    <div class="fill" style="width:' . (($sales_score / 3) * 100) . '%"></div>
                                </div>
                                <span>' . $monthly_sales . '</span>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . (($products_score / 2) * 100) . '%"></div>
                                    <span>' . number_format($num_products) . '</span>
                                </div>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . ($price_score * 100) . '%"></div>
                                    <span>$' . number_format($avg_price, 2) . '</span>
                                </div>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . ($reviews_score * 100) . '%"></div>
                                    <span>' . $avg_reviews . '</span>
                                </div>
                            </td>
                            <td>
                                <div class="bar">
                                    <div class="fill" style="width:' . ($rating_score * 100) . '%"></div>
                                    <span>' . round($avg_rating, 1) . '
                                </div>
                            </td>
                            <td>
                                <span>' . round($total_score, 2) . '</span>
                            </td>
                        </tr>';
            }
        }

        $table = $upgrade_notice . $membership_indicator . '<table class="table table-bordered table-striped table-hover" id="subcats" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Category Name</th>
                            <th>Avg. BSR</th>
                            <th>Est. Sales</th>
                            <th># Products</th>
                            <th>Avg. Price</th>
                            <th>Avg. Reviews</th>
                            <th>Avg. Rating</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>' .

                    $table .

                    '</tbody>
                </table>';

        echo $table;

    } else if($_POST['action'] == 'load_products') {

        $cat_id = $_POST['cat_id'];

        $asins = array();

        $conf = new GenericConfiguration();

        $conf
            ->setCountry('com')
            ->setAccessKey('AKIAJJ3K6XF7GAT5MM6Q')
            ->setSecretKey('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')
            ->setAssociateTag('azart-20');

        $apaiIo = new ApaiIO($conf);


        $search = new Search();
        $search->setCategory('Baby');
        $search->setBrowseNode($cat_id);
        $search->setResponseGroup(array('SalesRank', 'Reviews', 'Offers', 'Images', 'ItemAttributes'));
        $search->setPage(1);
        $search->setKeywords('-dsfdffddfdsf');


        $failed = false;
        $too_quick = false;

        do {

            try {
                
                $response = $apaiIo->runOperation($search);

                $failed = false;

                $xml = simplexml_load_string($response);

                if(isset($xml->Error)) {

                    $too_quick = true;

                    echo "Too quick...\r\n";
                
                } else {

                    $too_quick = false;
                }
            
            } catch (Exception $e) {
                
                sleep(1);

                $failed = true;

                continue;
            }

        } while($failed == true || $too_quick == true);

        $xml = simplexml_load_string($response);

        $count = 0;

        foreach ($xml->Items->Item as $item) { 

            //usleep(500000); // sleep half a second
            
            $asin = (string)$item->ASIN;

            array_push($asins, $asin);

            $item_name = $item->ItemAttributes->Title;

            if(isset($item->MediumImage->URL)) {

                $item_img = $item->MediumImage->URL;
            
            } else if(isset($item->SmallImage->URL)) {

                $item_img = $item->SmallImage->URL;
            }

            if(isset($item->SalesRank)) {

                $sales_rank = (string)$item->SalesRank;

            } else {

                $sales_rank = "N/A";
            }

            if(isset($item->Offers->Offer[0]->OfferListing->Price->Amount)) {

                $price = "$" . ((int)$item->Offers->Offer[0]->OfferListing->Price->Amount) / 100; 
            
            } else {

                $price = "N/A";
            }


            if($count == 0) {

                $data .= '<div class="row">
                            <div class="col-xs-1"></div>';

                //$data2 .= '<table class="table table-bordered table-striped table-hover" width="100%">';
            
            } else if($count == 5) {

                $data .= '</div>
                        <div class="row">
                            <div class="col-xs-1"></div>';
            }

            $data .= '<div class="col-xs-2">
                <div class="panel panel-default item" id="' . $asin . '">
                    <div class="panel-body">
                        <a href="http://www.amazon.com/dp/' . $asin . '/?tag=' . $affiliate_id . '" target="_blank" class="item_title" onclick="ga(\'send\', \'event\', \'Product\', \'Title\', \'' . $item_name . '\');"><strong>' . $item_name . '</strong></a>
                        <a href="http://www.amazon.com/dp/' . $asin . '/?tag=' . $affiliate_id . '" target="_blank" onclick="ga(\'send\', \'event\', \'Product\', \'Image\', \'' . $item_name . '\');"><img class="item_img" src="' . $item_img . '" /></a>
                    </div>
                    <div class="panel-footer item_details">
                        <div class="row">
                            <div class="col-xs-6">
                                Rank: <span>' . $sales_rank . '</span>
                            </div>
                            <div class="col-xs-6">
                                Price: <span>' . $price . '</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                Reviews: <span class="review_val"><img src="loading_small.gif" /></span>
                            </div>
                            <div class="col-xs-6">
                                Rating: <span class="rating_val"><img src="loading_small.gif" /></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';


            // $data2 .= '<tr>
            //             <td rowspan="2" class="img"><a href=""><img src="' . $item_img . '" width="max-width:100%; max-height:100%" /></a></td>
            //             <td colspan="4"><a href="" class="item_title"><strong>' . $item_name . '</strong></a></td>
            //         </tr>
            //         <tr>
            //             <td>Rank: <span>' . $sales_rank . '</span></td>
            //             <td>Price: <span>' . $price . '</span></td>
            //             <td>Reviews: <span>' . $num_reviews . '</span></td>
            //             <td>Rating: <span>' . $rating . '</span></td>
            //         </tr>';

            if($count == 9) {

                $data .= '</div>
                        </div>';

               ///$data2 .= '</table>';
            }

            $count++;
        }

        echo $data . '<input type="hidden" id="asins" value="' . implode(",", $asins) . '" />';

    }  else if($_POST['action'] == 'load_products_table') {

        $cat_id = $_POST['cat_id'];

        $asins = array();
        $bsrs = array();

        $conf = new GenericConfiguration();

        $conf
            ->setCountry('com')
            ->setAccessKey('AKIAJJ3K6XF7GAT5MM6Q')
            ->setSecretKey('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')
            ->setAssociateTag('azart-20');

        $apaiIo = new ApaiIO($conf);


        $search = new Search();
        $search->setCategory('Baby');
        $search->setBrowseNode($cat_id);
        $search->setResponseGroup(array('SalesRank', 'Reviews', 'Offers', 'Images', 'ItemAttributes'));
        $search->setPage(1);
        $search->setKeywords('-dsfdffddfdsf');


        $failed = false;
        $too_quick = false;

        do {

            try {
                
                $response = $apaiIo->runOperation($search);

                $failed = false;

                $xml = simplexml_load_string($response);

                if(isset($xml->Error)) {

                    $too_quick = true;

                    echo "Too quick...\r\n";
                
                } else {

                    $too_quick = false;
                }
            
            } catch (Exception $e) {
                
                sleep(1);

                $failed = true;

                continue;
            }

        } while($failed == true || $too_quick == true);

        $xml = simplexml_load_string($response);

        $count = 0;

        foreach ($xml->Items->Item as $item) { 

            //usleep(500000); // sleep half a second
            
            $asin = (string)$item->ASIN;

            array_push($asins, $asin);

            $item_name = $item->ItemAttributes->Title;

            if(isset($item->MediumImage->URL)) {

                $item_img = $item->MediumImage->URL;
            
            } else if(isset($item->SmallImage->URL)) {

                $item_img = $item->SmallImage->URL;
            }

            if(isset($item->SalesRank)) {

                $sales_rank = (string)$item->SalesRank;

            } else {

                $sales_rank = "N/A";
            }

            if(isset($item->Offers->Offer[0]->OfferListing->Price->Amount)) {

                $price = "$" . ((int)$item->Offers->Offer[0]->OfferListing->Price->Amount) / 100; 
            
            } else {

                $price = "N/A";
            }


            if($count == 0) {

                //$data .= '<div class="row">
                            //<div class="col-xs-1"></div>';

                //$data2 .= '<table class="table table-bordered table-striped table-hover" width="100%">';
            
            } else if($count == 5) {

                // $data .= '</div>
                //         <div class="row">
                //             <div class="col-xs-1"></div>';
            }

            // $data .= '<div class="col-xs-2">
            //     <div class="panel panel-default item" id="' . $asin . '">
            //         <div class="panel-body">
            //             <a href="http://www.amazon.com/dp/' . $asin . '/?tag=' . $affiliate_id . '" target="_blank" class="item_title" onclick="ga(\'send\', \'event\', \'Product\', \'Title\', \'' . $item_name . '\');"><strong>' . $item_name . '</strong></a>
            //             <a href="http://www.amazon.com/dp/' . $asin . '/?tag=' . $affiliate_id . '" target="_blank" onclick="ga(\'send\', \'event\', \'Product\', \'Image\', \'' . $item_name . '\');"><img class="item_img" src="' . $item_img . '" /></a>
            //         </div>
            //         <div class="panel-footer item_details">
            //             <div class="row">
            //                 <div class="col-xs-6">
            //                     Rank: <span>' . $sales_rank . '</span>
            //                 </div>
            //                 <div class="col-xs-6">
            //                     Price: <span>' . $price . '</span>
            //                 </div>
            //             </div>
            //             <div class="row">
            //                 <div class="col-xs-6">
            //                     Reviews: <span class="review_val"><img src="loading_small.gif" /></span>
            //                 </div>
            //                 <div class="col-xs-6">
            //                     Rating: <span class="rating_val"><img src="loading_small.gif" /></span>
            //                 </div>
            //             </div>
            //         </div>
            //     </div>
            // </div>';


            $data2 .= '<tr id="' . $asin . '">
                        <td>' . ($count + 1) . '</td>
                        <td style="text-align: left;"><a href="http://www.amazon.com/dp/' . $asin . '/?tag=' . $affiliate_id . '" target="_blank" class="item_title"><strong>' . $item_name . '</strong></a></td>
                        <td>' . $price . '</td>
                        <td>' . $sales_rank . '</td>
                        <td><span class="est_sales_val"><img src="loading_small.gif" /></span></td>
                        <td><span class="est_rev_val"><img src="loading_small.gif" /></span></td>
                        <td><span class="review_val"><img src="loading_small.gif" /></span></td>
                        <td><span class="rating_val"><img src="loading_small.gif" /></span></td>
                    </tr>';

            array_push($bsrs, $sales_rank);

            if($count == 9) {

                // $data .= '</div>
                //         </div>';

               //$data2 .= '</table>';
            }

            $count++;
        }

        echo '<table class="table table-bordered table-striped table-hover" width="100%" id="products">
                <tr>
                    <th>#</th>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Rank</th>
                    <th>Est. Sales</td>
                    <th>Est. Revenue</td>
                    <th># of Reviews</th>
                    <th>Rating</th>
                </tr>
                ' . $data2 . '</table>' . '<input type="hidden" id="asins" value="' . implode(",", $asins) . '" /><input type="hidden" id="bsrs" value="' . implode(",", $bsrs) . '" />';

    } else if($_POST['action'] == 'load_product_reviews_ratings') {

        $asins = $_POST['asins'];
        $asins = explode(",", $asins);

        require_once('simple_html_dom.php');

        $proxies = file('ppx.txt');

        foreach ($proxies as $key => $proxy) {
            
            $proxies[$key] = array(trim($proxy), 0);
        }

        $data = "";

        foreach ($asins as $asin) {

            $data .= $asin . ',';

            $review_url = 'http://www.amazon.com/gp/customer-reviews/widgets/average-customer-review/popover/ref=dpx_acr_pop_?contextId=dpx&asin=' . $asin;

            $html = "";
            $tries = 0;

            do {
                
                $fetch_failed = false;

                $rand_key = array_rand($proxies);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $review_url);
                curl_setopt($ch, CURLOPT_PROXY, $proxies[$rand_key][0]);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
                curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);

                $html = str_get_html($curl_scraped_page);

                if(is_object($html)) {

                    //$html = file_get_html($review_url);

                    if(!($html->find('span[class=a-size-base]', 0))) {

                        $fetch_failed = true;

                        //echo "Fetching Reviews failed... " . $proxies[$rand_key][0] . "\r\n";

                        $proxies[$rand_key][1] += 1;

                        //echo $curl_scraped_page; exit;
                    }

                } else {

                    $fetch_failed = true;

                    //echo "Fetching Reviews failed... " . $proxies[$rand_key][0] . "\r\n";

                    $proxies[$rand_key][1] += 1;

                    //echo $curl_scraped_page; exit;
                }

                $tries++;

            } while($fetch_failed == true && $tries <= 20);
            file_put_contents(__DIR__.'/test.log', $tries . "\n\n", FILE_APPEND);
            if(!$fetch_failed) { //success
                
                $rating = $html->find('span[class=a-size-base]', 0)->plaintext;
                $rating = trim($rating);
                $rating = (float)substr($rating, 0, strpos($rating, ' out of'));
                
                $num_reviews = $html->find('a[class=a-size-small a-link-emphasis]', 0)->plaintext;

                if (strpos($num_reviews, 'both') !== false) { //2 reviews

                    $num_reviews = 2;
                
                } else {

                    $num_reviews = trim($num_reviews);
                    $num_reviews = substr($num_reviews, strpos($num_reviews, 'See all ') + 8);
                    $num_reviews = intval(str_replace(",", "", substr($num_reviews, 0, strpos($num_reviews, ' reviews'))));
                }

                $data .= $num_reviews . ',' . $rating . ';';
            
            } else { //fetch failed

                $data .= 'N/A,N/A;';
            }

        }

        echo substr($data, 0, -1);

    } else if($_POST['action'] == 'load_product_est_sales') { 

        $main_cat_id = $_POST['main_cat_id'];
        $bsrs = $_POST['bsrs'];
        $bsrs = explode(",", $bsrs);
        $asins = $_POST['asins'];
        $asins = explode(",", $asins);

        $est_sales = '';

        $proxies = file('ppx.txt');

        foreach ($proxies as $key => $proxy) {
            
            $proxies[$key] = array(trim($proxy), 0);
        }


        $main_cats = array(
            "1000" => "Books",
            "1063498" => "Home+%26+Kitchen",
            "1084128" => "Office+Products",
            "11055981" => "Beauty",
            "11965861" => "Musical+Instruments",
            "15690151" => "Automotive",
            "16310161" => "Industrial+%26+Scientific",
            "16310211" => "Grocery+%26+Gourmet+Food",
            "165795011" => "Toys+%26+Games",
            "165797011" => "Baby",
            "2335753011" => "Cell+Phones+%26+Accessories",
            "2617942011" => "Arts%2C+Crafts+%26+Sewing",
            "2619526011" => "Appliances",
            "2619534011" => "Pet+Supplies",
            "3238155011" => "Patio%2C+Lawn+%26+Garden",
            "3375301" => "Sports+%26+Outdoors",
            "3760931" => "Health+%26+Personal+Care",
            "468240" => "Home+Improvement",
            "493964" => "Electronics",
            "541966" => "Computers",
            "7141124011" => "Clothing"
            // "7141124011_shoes" => "Shoes",
            // "7141124011_jewelry" => "Jewelry"
        );
    
        $main_cat = $main_cats[$main_cat_id];

        $count = 0;
        
        for ($i = 0; $i < count($asins); $i++) { 

            $num_sales = '';

            $avg_sales_rank = $bsrs[$i];

            if($avg_sales_rank != "N/A" && $avg_sales_rank > 0) {

                $rand_key = array_rand($proxies);

                $url = 'https://junglescoutpro.herokuapp.com/api/v1/est_sales?store=us&rank=' . $avg_sales_rank . '&category=' . $main_cat;

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_PROXY, $proxies[$rand_key][0]);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_REFERER, 'http://www.junglescout.com/estimator/');
                //curl_setopt($ch, CURLOPT_HEADER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
                curl_setopt($ch, CURLOPT_TIMEOUT, 10); //timeout in seconds
                
                $result = json_decode(curl_exec($ch), true);
                curl_close($ch);

                $num_sales = $result["estSalesResult"];

                $est_sales .= $asins[$i] . ',' . $num_sales . ';';

                $count++;

            } else {

                $est_sales .= $asins[$i] . ',N/A;';
            }
        }

        echo substr($est_sales, 0, -1);

    } else if($_POST['action'] == 'process_payment') {

        $txn_id = $_POST['txn_id'];
        $user_id = $_SESSION['user_id'];

        DEFINE ('DBUSER', 'remote');
        DEFINE ('DBPASS', 'GQ8XEQAVgwvVL8EpTUCaVS6S');
        DEFINE ('DBHOST', '107.170.7.163');
        DEFINE ('DBNAME', 'niche_finder');

        $dbo = new PDO('mysql:host=' .DBHOST. ';dbname=' .DBNAME, DBUSER, DBPASS);

        $found = false;
        $count = 0;

        do {    

            $query = $dbo->prepare("SELECT * FROM transactions WHERE user_id = ? AND txn_id = ?");
            $query->execute(array($user_id, $txn_id));

            $result = $query->fetchAll();

            foreach($result as $row) {

                $found == true;

                $query2 = $dbo->prepare("SELECT expiry_date FROM users WHERE id = ?");
                $query2->execute(array($user_id));

                $result2 = $query2->fetchAll();

                foreach($result2 as $row2) {

                    $expiry_date = $row2[0];
                }

                $_SESSION['membership_status'] = 1;
                $_SESSION['expiry_date'] = $expiry_date;

                echo 'success';
                exit;
            }

            sleep(1);

            $count++;

        } while($found == false && $count < 30);

        echo 'failed';
        exit;
    }


?>